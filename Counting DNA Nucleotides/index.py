import tkinter as tk


adenine = gunine = tymin = cytozin = 0


def translate(entry):
    output.delete("1.0", tk.END)
    entry = entry.upper()
    adenine = gunine = tymin = cytozin = 0
    for char in entry:
        if char == 'A':
            adenine += 1
        elif char == 'G':
            gunine += 1
        elif char == 'T':
            tymin += 1
        elif char == 'C':
            cytozin += 1

    output.insert(0.1, "T:" + str(tymin) + "  ")
    output.insert(0.1, "G:" + str(gunine) + "  ")
    output.insert(0.1, "C:" + str(cytozin) + "  ")
    output.insert(0.1, "A:" + str(adenine) + "  ")


root = tk.Tk()
root.title("Rosalind 1")
canvas = tk.Canvas(root, height="400", width="600")
canvas.pack()
frame = tk.Frame(root)
frame.place(relx="0.02", rely="0.05", relwidth="0.8", relheight="0.1")
entry = tk.Entry(frame)
button = tk.Button(frame, text="OK", bg="white", command=lambda: translate(entry.get()))
label = tk.Label(frame, text="Input DNA string here:")

output = tk.Text(frame, width="100", height="100")

label.pack(side="left")
entry.pack(side="left", fill="x", expand="true")
button.pack(side="left")
output.pack(side="left", padx="5")
root.mainloop()

