import re


def countRabbitPairs(n, k):

    babiesForRemoval = []
    breedable = []  # breadable:False == teraz dospel
    babies = [False]

    for mesiac in range(int(n)):  # babies:
        for x in babies:  # True == babo zije 1 mesiac
            if x:         # False == babo je malicke
                breedable.append(False)
                babiesForRemoval.append(babies.index(x))
            else:
                babies[babies.index(x)] = True
        # zajaciky zo zoznamu na vymazanie sa realne vymazu
        for y in babiesForRemoval:
            babies.pop(y)
        babiesForRemoval.clear()
        # ak je zajac dospely, zajaciatka v pocte k sa pridaju do zoznamu zajaciatok
        for y in breedable:
            if y:
                for j in range(int(k)):
                    babies.append(True)
            else: #ak zajac nie je dospely, tak dospeje
                breedable[breedable.index(y)] = True
        print(str(mesiac)+'. '+str(len(babies)+len(breedable)))


s = input("Give us n and k:")
p = re.findall('\d+', s)
countRabbitPairs(p[0], p[1])
