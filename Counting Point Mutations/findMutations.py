file = open("rosalind_hamm.txt", "r")
s1 = file.readline().replace('\n', '')
s2 = file.readline()
differences = 0
for x in range(len(s1)):
    if s1[x] != s2[x]:
        differences += 1

print(differences)
