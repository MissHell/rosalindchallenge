import re

file = open("rosalind_iprb.txt", "r")
s = file.readline()
k, m, n = re.findall('\d+', s)
k = int(k)
m = int(m)
n = int(n)
all = k + m + n
allcomb = (all - 1) * all * 4
dom = k * 4 * (all - 1) + m * (4 * k + 3*(m-1) + 2 * n) + n * (2 * m + 4 * k)
print(dom)
print(allcomb)
print(format(dom / allcomb, '.5f'))
