s = input('Input DNA string: ')
s = s.upper()
s = s[::-1]
newS = ''
for chr in s:
    if chr == 'A':
        newS = newS+'T'
    elif chr == 'T':
        newS = newS+'A'
    elif chr == 'C':
        newS = newS+'G'
    elif chr == 'G':
        newS = newS+'C'

print(newS)
