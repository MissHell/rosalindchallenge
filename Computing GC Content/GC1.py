file = open(r"rosalind_gc.txt", "r")
codeNames = []
dnaStrings = []
counts = []
s = file.read().split('>')
file.close()

highestGCcontent = 0
highestGCindex = 0
s.pop(0)

for x in s:
    x = x.replace('\n', '')
    codeNames.append(x[:13])
    dnaStrings.append(x[13:])

for dnaString in dnaStrings:
    cgCount = 0
    percentage = 0.0
    for chr in dnaString:
        if chr == 'C' or chr == 'G':
            cgCount += 1
    percentage = 100*cgCount/len(dnaString)
    counts.append([cgCount, round(percentage, 6)])

for per in counts:
    if per[1] > highestGCcontent:
        highestGCcontent = per[1]
        highestGCindex = counts.index(per)

print(codeNames[highestGCindex])
print(format(counts[highestGCindex][1], '.6f'))





