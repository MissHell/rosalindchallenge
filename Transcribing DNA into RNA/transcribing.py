import tkinter as tk


def transcription(entry):

    entry = entry.upper()
    RNA = entry.replace('T','U')
    output.insert(1.0, RNA)


root = tk.Tk()
root.title("Rosalind 2")

frame = tk.Frame()
frame.place(relx="0.1", rely="0.1", relwidth="0.8", relheight="0.8")
entry = tk.Entry(frame)
button = tk.Button(frame, text="OK", bg="green", command=lambda: transcription(entry.get()))
output = tk.Text(frame)

entry.pack()
button.pack()
output.pack()
root.mainloop()
